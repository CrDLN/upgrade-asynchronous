/*1.1 Utiliza esta url de la api Agify 'https://api.agify.io?name=michael' para 
hacer un .fetch() y recibir los datos que devuelve. Imprimelo mediante un 
console.log(). Para ello, es necesario que crees un .html y un .js.*/

const getUser = async()=>{
    const result = await fetch(`https://api.agify.io?name=michael`);
    const resultJson = await result.json();
    console.log(resultJson);
    return resultJson;
}
getUser();

/*2.1 Dado el siguiente javascript y html. Añade la funcionalidad necesaria usando 
fetch() para hacer una consulta a la api cuando se haga click en el botón, 
pasando como parametro de la api, el valor del input.*/
const baseUrl = 'https://api.nationalize.io?name={aqui va el nombre}';


const getUserInformation = async()=>{
    const $$name = document.querySelector('input');
    const result = await fetch(`https://api.nationalize.io?name=${$$name.value}`);
    const resultJson = await result.json();
    console.log(resultJson);
    //Falta hacer el bucle.
    console.log('El nombre '+ resultJson.name +' tiene un ' + resultJson.country[1].country_id + ' con un % de ' + resultJson.country[1].probability);
    
    //CREACION DE ELEMENTO
    const node = document.body.querySelector('div1');
    const element = document.createElement('p');
    element.innerHTML = 'hola';

    const $$delete = document.createElement('button');
    $$delete.id = 'btnDeleteP'
    $$delete.innerHTML = 'X'

    element.appendChild($$delete);
    document.body.insertBefore(element,node);

}

const $$button = document.querySelector('button').addEventListener('click',getUserInformation)


/*2.3 En base al ejercicio anterior. Crea dinamicamente un elemento  por cada petición 
a la api que diga...'El nombre X tiene un Y porciento de ser de Z' etc etc.
EJ: El nombre Pepe tiene un 22 porciento de ser de ET y un 6 porciento de ser 
de MZ.*/

//console.log('El nombre '+ resultJson.name +' tiene un ' + resultJson.country[1].country_id + ' con un % de ' + resultJson.country[1].probability);

/*2.4 En base al ejercicio anterior, crea un botón con el texto 'X' para cada uno 
de los p que hayas insertado y que si el usuario hace click en este botón 
eliminemos el parrafo asociado.*/

//mirar, hecho en otro ejercicio

const deleteP = function(){
    console.log('hola');
}

const $$deleteBtn = document.querySelector('#btnDeleteP').addEventListener('click', deleteP);

